import { Selector } from 'testcafe';

fixture`Main Page`
    .page`http://localhost:4200`;

test('Test View Details Button', async t => {
    await t
        .click('#view-details3-btn')
        .expect(Selector('h2').innerText).eql('Study Japanese');
});

test('Test Delete Button - Cancel', async t => {
    await t
        .setNativeDialogHandler(() => false)
        .click('#view-details3-delete-btn')
        .expect(Selector('#view-details3-delete-btn').exists).eql(true);
});

test('Test Delete Button - OK', async t => {
    await t
        .setNativeDialogHandler(() => true)
        .click('#view-details3-delete-btn')
        .expect(Selector('#view-details3-delete-btn').exists).eql(false);
});

test('Test Create New Item Button', async t => {
    await t
        .click('#create-new-item-btn')
        .expect(Selector('#title').exists).eql(true);
});
