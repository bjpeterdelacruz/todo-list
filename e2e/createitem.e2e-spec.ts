import { Selector } from 'testcafe';

fixture`Create New Item Page`
    .page`http://localhost:4200`;

test('Test Create New Item Button - should be enabled', async t => {
    await t
        .click('#create-new-item-btn')
        .typeText("#title", "This is a test")
        .typeText("#description", "This is a description")
        .expect(Selector("#create-btn").getAttribute("disabled")).eql(null);
});

test('Test Create New Item Button - no description, should be disabled', async t => {
    await t
        .click('#create-new-item-btn')
        .typeText("#title", "This is a test")
        .expect(Selector("#create-btn").getAttribute("disabled")).eql("");
});

test('Test Create New Item Button - no title, should be disabled', async t => {
    await t
        .click('#create-new-item-btn')
        .typeText("#description", "This is a description")
        .expect(Selector("#create-btn").getAttribute("disabled")).eql("");
});

test('Test Cancel Button - should return to home page', async t => {
    await t
        .click('#create-new-item-btn')
        .click("#cancel-btn")
        .expect(Selector("h2").innerText).eql("Todo List");
});
