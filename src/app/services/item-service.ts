import { Injectable } from '@angular/core';
import { Item } from '../models/item';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ItemService {

  serverUrl = "http://localhost:3000/items";

  constructor(private httpClient: HttpClient) { }

  getItems(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(this.serverUrl);
  }

  getItem(id: string): Observable<Item> {
    return this.httpClient.get<Item>(`${this.serverUrl}/${id}`);
  }

  markItem(item: Item): Observable<Item> {
    return this.httpClient.put<Item>(`${this.serverUrl}/${item.id}`,
      { id: item.id, title: item.title, description: item.description, isCompleted: item.isCompleted });
  }

  deleteItem(id: number): Observable<Item> {
    return this.httpClient.delete<Item>(`${this.serverUrl}/${id}`);
  }

  addItem(title: string, description: string, isCompleted: boolean): Observable<Item> {
    return this.httpClient.post<Item>(this.serverUrl,
      { title: title, description: description, isCompleted: isCompleted});
  }
}
