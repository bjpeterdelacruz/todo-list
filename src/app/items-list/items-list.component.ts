import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from '../models/item';
import { ItemService } from '../services/item-service';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit {

  items: Item[] = [];

  constructor(private router: Router, private itemService: ItemService) {
  }

  ngOnInit(): void {
    this.itemService.getItems().subscribe(items => {
      this.items = items;
    });
  }

  markItem(event: any, item: Item) {
    item.isCompleted = event.target.checked;
    this.itemService.markItem(item).subscribe(_ => {
      console.log("Success!");
    });
  }
  viewDetails(item: Item) {
    this.router.navigate(['/item', item.id]);
  }

  deleteItem(id: number) {
    if (!confirm('Are you sure?')) {
        return;
    }
    this.itemService.deleteItem(id).subscribe(_ => {
      this.itemService.getItems().subscribe(items => {
        this.items = items;
      });
    });
  }

  createNewItem() {
    this.router.navigate(['/create']);
  }
}
