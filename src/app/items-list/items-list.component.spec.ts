import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientModule } from '@angular/common/http';
import { ItemsListComponent } from './items-list.component';
import { Item } from '../models/item';
import { Observable, of } from 'rxjs';
import { ItemService } from '../services/item-service';
import { Router } from '@angular/router';

describe('ItemsListComponent', () => {
  let testItem: Item = { id: 1, title: '', description: '', isCompleted: false};

  class MockItemService {
    getItems(): Observable<Item[]> {
      return of([testItem]);
    }

    markItem(item: Item): Observable<Item> {
      return of({ id: item.id, title: item.title, description: item.description, isCompleted: item.isCompleted});
    }
  }

  let router: Router;
  let component: ItemsListComponent;
  let fixture: ComponentFixture<ItemsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemsListComponent ],
      providers: [
        {provide: ItemService, useClass: MockItemService}
      ],
      imports: [RouterTestingModule.withRoutes(
        [
          {path: 'item', component: ItemsListComponent},
          {path: 'create', component: ItemsListComponent}
        ]
      ), HttpClientModule]
    })
    .compileComponents();

    router = TestBed.get(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to /item upon success', () => {
    const routerSpy = spyOn(router, 'navigate');
    component.viewDetails(testItem);
    expect(routerSpy).toHaveBeenCalledWith(['/item', testItem.id]);
  });

  it('should navigate to /create upon success', () => {
    const routerSpy = spyOn(router, 'navigate');
    component.createNewItem();
    expect(routerSpy).toHaveBeenCalledWith(['/create']);
  });

  it('should console out "Success!" upon marking an item', () => {
    const consoleSpy = spyOn(console, 'log');
    const event = {
      target: {
        checked: true
      }
    };
    component.markItem(event, testItem);
    expect(consoleSpy).toHaveBeenCalledTimes(1);
  });
});
