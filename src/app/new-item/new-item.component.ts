import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from '../services/item-service';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {

  title: string = '';
  description: string = '';
  isCompleted: boolean = false;

  constructor(private router: Router, private itemService: ItemService) { }

  ngOnInit(): void {
  }

  goBack() {
    this.router.navigate(['/items'])
  }

  createItem() {
    this.itemService.addItem(this.title, this.description, this.isCompleted).subscribe(
      {
        next: () => this.router.navigate(['/items']),
        error: error => console.log("This is an error: " + error)
      }
    );
  }
}
