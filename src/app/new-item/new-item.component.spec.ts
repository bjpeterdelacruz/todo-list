import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { ItemsListComponent } from '../items-list/items-list.component';
import { ItemService } from '../services/item-service';

import { NewItemComponent } from './new-item.component';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { Item } from '../models/item';

describe('NewItemComponent', () => {
  class MockItemService {
    addItem(title: string, description: string, isCompleted: boolean): Observable<Item> {
      return of({ id: 1, title: title, description: description, isCompleted: isCompleted});
    }
  }

  let router: Router;
  let component: NewItemComponent;
  let fixture: ComponentFixture<NewItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewItemComponent ],
      providers: [
        {provide: ItemService, useClass: MockItemService}
      ],
      imports: [HttpClientModule,
        RouterTestingModule.withRoutes(
          [{path: 'items', component: ItemsListComponent}]
        )
      ]
    })
    .compileComponents();

    router = TestBed.get(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to /items upon success', () => {
    const routerSpy = spyOn(router, 'navigate');
    component.createItem();
    expect(routerSpy).toHaveBeenCalledWith(['/items']);
  });
});


describe('NewItemComponent - with failed addItem', () => {
  class MockItemService {
    addItem(title: string, description: string, isCompleted: boolean): Observable<Item> {
      return throwError(() => new Error("Something bad happened."));
    }
  }

  let router: Router;
  let component: NewItemComponent;
  let fixture: ComponentFixture<NewItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewItemComponent ],
      providers: [
        {provide: ItemService, useClass: MockItemService}
      ],
      imports: [HttpClientModule,
        RouterTestingModule.withRoutes(
          [{path: 'items', component: ItemsListComponent}]
        )
      ]
    })
    .compileComponents();

    router = TestBed.get(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should NOT navigate to /items upon failure', () => {
    const routerSpy = spyOn(router, 'navigate');
    component.createItem();
    expect(routerSpy).toHaveBeenCalledTimes(0);
  });
});
