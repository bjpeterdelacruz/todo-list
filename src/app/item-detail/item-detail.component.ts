import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Item } from '../models/item';
import { ItemService } from '../services/item-service';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {

  item: Item = {id: 0, title: '', description: '', isCompleted: false};

  constructor(private route: ActivatedRoute, private router: Router, private itemService: ItemService) { }

  ngOnInit(): void {
    this.itemService.getItem(this.route.snapshot.paramMap.get('id')!).subscribe(item => {
      this.item = item;
    });
  }

  markCompleted() {
    this.itemService.markItem(this.item).subscribe({
      next: () => this.goBack()
    });
  }

  goBack() {
    this.router.navigate(['/items']);
  }
}
