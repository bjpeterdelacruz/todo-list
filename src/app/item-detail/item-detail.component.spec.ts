import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientModule } from '@angular/common/http';
import { ItemDetailComponent } from './item-detail.component';
import { ItemsListComponent } from '../items-list/items-list.component';
import { ItemService } from '../services/item-service';
import { Item } from '../models/item';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

class MockItemService {
  getItem(id: string): Observable<Item> {
    return of({ id: 1, title: '', description: '', isCompleted: false});
  }

  markItem(item: Item): Observable<Item> {
    return of({ id: 1, title: item.title, description: item.description, isCompleted: item.isCompleted});
  }
}

describe('ItemDetailComponent', () => {
  let router: Router;
  let component: ItemDetailComponent;
  let fixture: ComponentFixture<ItemDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemDetailComponent ],
      providers: [
        {provide: ItemService, useClass: MockItemService}
      ],
      imports: [RouterTestingModule, HttpClientModule,
        RouterTestingModule.withRoutes(
          [{path: 'items', component: ItemsListComponent}]
        )]
    })
    .compileComponents();

    router = TestBed.get(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.item).toEqual({id: 1, title: '', description: '', isCompleted: false});
  });

  it('should navigate to /items when user clicks Go Back button', () => {
    const routerSpy = spyOn(router, 'navigate');
    component.goBack();
    expect(routerSpy).toHaveBeenCalledWith(['/items']);
  });

  it('should navigate to /items when user marks item as completed', () => {
    const routerSpy = spyOn(router, 'navigate');
    component.markCompleted();
    expect(routerSpy).toHaveBeenCalledWith(['/items']);
  });
});
